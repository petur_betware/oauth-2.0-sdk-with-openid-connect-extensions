version 1.0 (2012-05-29)
    * First official release with authorisation endpoint, token endpoint, check 
      ID endpoint and UserInfo endpoint support.
    * JSON Web Tokens (JWTs) support through the Nimbus-JWT library.
    * Language Tags (RFC 5646) support through the Nimbus-LangTag library.
    * JSON support through the JSON Smart library.


version 2.0 (2013-05-13)
    * Intermediary development release with Maven build, published to 
      Maven Central.

version 2.1 (2013-06-06)
    * Updates the APIs to OpenID Connect Messages draft 20, OpenID Connect 
      Standard draft 21, OpenID Connect Discovery draft 17 and OpenID 
      Connect Registration draft 19.
    * Major refactoring of the APIs for greater simplicity.
    * Adds JUnit tests.
    
version 2.2 (2013-06-18)
    * Refactors dynamic OpenID Connect client registration.
    * Adds partial support of the OAuth 2.0 Dynamic Client Registration 
      Protocol (draft-ietf-oauth-dyn-reg-12).
    * Optimises parsing of request parameters consisting of one or more
      tokens (scope, response type, etc).

version 2.3 (2013-06-19)
    * Renames OAuth 2.0 dynamic client registration package.
    * Adds ClientInformation.getClientMetadata() method.
    * Adds OIDCClientInformation class.

version 2.4 (2013-06-20)
    * Adds static OIDCClientInformation.parse(JSONObject) method.

version 2.5 (2013-06-22)
    * Adds support OAuth 2.0 dynamic client update.
    * Adds OpenID Connect dynamic client registration classes.

version 2.6 (2013-06-25)
    * Enforces order of preference of ACR values in OpenID Connect client
      metadata, as required by the specification.
    * Documentation and performance improvements.

version 2.7 (2013-06-26)
    * Switches Identifier generation to java.security.SecureRandom.

version 2.8 (2013-06-30)
    * Fixes serialisation and assignment bugs in ClientMetadata.
    * Switches Secret generation to java.security.SecureRandom.

version 2.9 (2013-09-17)
    * Changes the licensing terms to Apache 2.0.
    * Updates the APIs to draft-ietf-oauth-dyn-reg-14.
    * Separates the authorisation grant from the token request.
    * Refactors client authentication.
    * Numerous bug fixes and improvements.
    * Upgrades dependencies.

version 2.10 (2013-09-22)
    * Refactors OpenID Connect token response classes.
    * Adds implicit grant type constant.
    * Upgrades Nimbus JOSE+JWT dependency.

version 2.11 (2013-10-07)
    * Adds ClaimsSet.addAll and UseInfo.addAll methods.
    * Adds Audience.toSingleAudienceList method.
    * Renames ACRRequest empty check method.
    * Refactors GeneralException and extending classes.
    * Upgrades Nimbus JOSE+JWT dependency.
    
version 2.12 (2013-10-21)
    * Refactors ClaimsRequest resolution methods, fixes bugs that affected the
      correct parsing, serialisation and proper  ID token claims redirection.
    * Upgrades Nimbus JOSE+JWT dependency.

version 2.13 (2013-10-21)
    * Adds helper static ClaimsRequest.resolve(ResponseType, Scope,
      ClaimsRequest) method.

version 2.14 (2014-01-14)
    * Renames classes and methods in openid package to reflect terminology
      changes in the latest round of the OpenID Connect drafts.
    * Adds builder to OAuth 2.0 authorisation request and OpenID Connect
      authentication request classes.
    * Fixes fragment encoding of OpenID Connect authentication responses that
      contain an ID token.
    * Adds support for "none" response type value.
    * Fixes URL reconstruction with IPv6 address in HTTPRequest.
    * Adds short-hand methods to Scope and ResponseType classes.
    * Adds support for OpenID Connect registration parameters introduced in the
      latest round of the OpenID Connect drafts.
    * Extends JUnit tests, fixes numerous discovered bugs.

version 2.14.1 (2014-01-14)
    * Upgrades to Nimbus LangTag 1.4.

version 2.14.2 (2014-01-17)
    * Upgrades to Nimbus JOSE+JWT 2.22.1.

version 2.15 (2014-02-10)
    * Adds WWW-Authenticate response header for client error responses.
    * Refactors BearerAccessToken parsing from HTTP request.

version 2.15.1 (2014-02-11)
    * Fixes HTTPRequest.send throwing IOException on 4xx status code.

version 2.15.2 (2014-02-12)
    * Fixes content retrieval in HTTPRequest.send for HTTP 4xx errors.
    * Token error parsing supports general HTTP error codes.
    * TokenErrorResponse.toHTTPRequest can now handle null error objects.

version 2.15.3 (2014-03-08)
    * Ensures case insensitive comparison of access token types.
    * Updates the OpenID Connect specification references to the final 1.0
      release from 2014-02-25.

version 3.0 (2014-03-27)
    * Replaces java.net.URL representations with java.net.URI where URL is not
      directly required (see issues 79 and 80).

version 3.0.1 (2014-04-08)
    * Upgrades Nimbus JOSE + JWT dependency to 2.24.
    * Upgrades Apache Commons Lang dependency to 3.3.1.
    * Upgrades Apache Commons Codec dependency to 1.9.

version 3.0.2 (2014-04-23)
    * Upgrades Nimbus JOSE + JWT dependency to 2.25.

version 3.1 (2014-04-24)
    * Adds static ClientAuthenticationMethod.parse(String) method.

version 3.2 (2014-04-28)
    * Upgrades source and compile target to Java 7.
    * Fixes NullPointerException in OIDCProviderMetadata.toJSONObject() method.