package com.nimbusds.openid.connect.sdk;


import com.nimbusds.oauth2.sdk.Response;


/**
 * OpenID Connect authentication response.
 *
 * <p>Related specifications:
 *
 * <ul>
 *     <li>OpenID Connect Core 1.0, sections 3.1.2.5. and 3.1.2.6.
 * </ul>
 */
public interface AuthenticationResponse extends Response { }
